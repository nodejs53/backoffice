<?php

namespace App\Http\Controllers;

use App\Editeur;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class EditeurController extends Controller{


    public function index(){

        $editeurs  = Editeur::all();

        return response()->json($editeurs);

    }

    public function getEditeur($id){

        $editeur  = Editeur::find($id);

        return response()->json($editeur);
    }

    public function saveEditeur(Request $request){

        $editeur = Editeur::create($request->all());

        return response()->json($editeur);

    }

    public function deleteEditeur($id){
        $editeur  = Editeur::find($id);

        $editeur->delete();

        return response()->json('success');
    }

    public function updateEditeur(Request $request,$id){
        $editeur  = Editeur::find($id);

        $editeur->name = $request->input('name');
        $editeur->password = $request->input('password');

        $editeur->save();

        return response()->json($editeur);
    }

}
