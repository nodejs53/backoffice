<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function() use ($app) {
    return $app->welcome();
});

$app->get('/api/editeur','EditeurController@index');

$app->get('/api/Editeur/{id}','EditeurController@getEditeur');

$app->post('/api/Editeur','EditeurController@saveEditeur');

$app->put('/api/Editeur/{id}','EditeurController@updateEditeur');

$app->delete('/api/Editeur/{id}','EditeurController@deleteEditeur');
